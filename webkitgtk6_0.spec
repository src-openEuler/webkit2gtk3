%undefine __cmake_in_source_build

# Filter out provides for private libraries
%global __provides_exclude_from ^(%{_libdir}/webkitgtk-6\\.0/.*\\.so)$

# Run dwz to reduce debuginfo package size
%global _dwz_max_die_limit 250000000
%global _find_debuginfo_dwz_opts --run-dwz\\\
        --dwz-max-die-limit %{_dwz_max_die_limit}

%global add_to_license_files() \
        mkdir -p _license_files ; \
        cp -p %1 _license_files/$(echo '%1' | sed -e 's!/!.!g')

# Clang is preferred by skia
%global toolchain clang

# Build document by default
%bcond_without docs

# Not support GamePad by default
%bcond_with gamepad

Name:           webkitgtk6.0
Version:        2.46.6
Release:        2
Summary:        GTK web content engine library
License:        BSD-3-Clause AND LGPL-2.0-or-later
URL:            https://www.webkitgtk.org/
Source0:        https://webkitgtk.org/releases/webkitgtk-%{version}.tar.xz

Patch1000:      webkitgtk-add-loongarch-and-sw.patch

Patch6000:      backport-CVE-2025-24201.patch

#Dependency
BuildRequires:  bison
BuildRequires:  bubblewrap
BuildRequires:  clang
BuildRequires:  cmake
BuildRequires:  flex
BuildRequires:  gettext
BuildRequires:  gi-docgen
BuildRequires:  git
BuildRequires:  gnupg2
BuildRequires:  gperf
BuildRequires:  hyphen-devel
BuildRequires:  libatomic
BuildRequires:  ninja-build
BuildRequires:  openssl-devel
BuildRequires:  perl(bigint)
BuildRequires:  perl(English)
BuildRequires:  perl(FindBin)
BuildRequires:  perl(JSON::PP)
BuildRequires:  python3
BuildRequires:  ruby
BuildRequires:  rubygems
BuildRequires:  rubygem-json
BuildRequires:  unifdef
BuildRequires:  xdg-dbus-proxy

BuildRequires:  pkgconfig(atspi-2)
BuildRequires:  pkgconfig(cairo)
BuildRequires:  pkgconfig(egl)
BuildRequires:  pkgconfig(enchant-2)
BuildRequires:  pkgconfig(epoxy)
BuildRequires:  pkgconfig(fontconfig)
BuildRequires:  pkgconfig(freetype2)
BuildRequires:  pkgconfig(gbm)
BuildRequires:  pkgconfig(glib-2.0)
BuildRequires:  pkgconfig(gobject-introspection-1.0)
BuildRequires:  pkgconfig(gstreamer-1.0)
BuildRequires:  pkgconfig(gstreamer-plugins-base-1.0)
BuildRequires:  pkgconfig(gtk4)
BuildRequires:  pkgconfig(harfbuzz)
BuildRequires:  pkgconfig(icu-uc)
BuildRequires:  pkgconfig(lcms2)
BuildRequires:  pkgconfig(libdrm)
BuildRequires:  pkgconfig(libgcrypt)
BuildRequires:  pkgconfig(libjpeg)
BuildRequires:  pkgconfig(libnotify)
BuildRequires:  pkgconfig(libpng)
BuildRequires:  pkgconfig(libseccomp)
BuildRequires:  pkgconfig(libsecret-1)
BuildRequires:  pkgconfig(libsoup-3.0)
BuildRequires:  pkgconfig(libsystemd)
BuildRequires:  pkgconfig(libtasn1)
BuildRequires:  pkgconfig(libwebp)
BuildRequires:  pkgconfig(libwoff2dec)
BuildRequires:  pkgconfig(libxslt)
%if %{with gamepad}
BuildRequires:  pkgconfig(manette-0.2)
%endif
BuildRequires:  pkgconfig(sqlite3)
BuildRequires:  pkgconfig(sysprof-capture-4)
BuildRequires:  pkgconfig(upower-glib)
BuildRequires:  pkgconfig(wayland-client)
BuildRequires:  pkgconfig(wayland-egl)
BuildRequires:  pkgconfig(wayland-protocols)
BuildRequires:  pkgconfig(wayland-server)
BuildRequires:  pkgconfig(xt)

Requires:       javascriptcoregtk6.0%{?_isa} = %{version}-%{release}
Requires:       bubblewrap
Requires:       libGLES
Requires:       xdg-dbus-proxy
Recommends:     geoclue2
Recommends:     gstreamer1-plugins-bad-free
Recommends:     gstreamer1-plugins-good
Recommends:     xdg-desktop-portal-gtk
Provides:       bundled(angle)
Provides:       bundled(pdfjs)
Provides:       bundled(skia)
Provides:       bundled(xdgmime)
Obsoletes:      webkit2gtk5.0 < %{version}-%{release}

%description
WebKitGTK is the port of the WebKit web rendering engine to the
GTK platform. This package contains WebKitGTK for GTK 4 and libsoup 3.

%package -n     webkitgtk6.0-devel
Summary:        Development files for webkitgtk6.0
Requires:       webkitgtk6.0%{?_isa} = %{version}-%{release}
Requires:       javascriptcoregtk6.0%{?_isa} = %{version}-%{release}
Requires:       javascriptcoregtk6.0-devel%{?_isa} = %{version}-%{release}
Obsoletes:      webkit2gtk5.0-devel < %{version}-%{release}

%description -n webkitgtk6.0-devel
The webkitgtk6.0-devel package contains libraries, build data, and header
files for developing applications that use webkitgtk6.0.

%if %{with docs}
%package -n     webkitgtk6.0-help
Summary:        Documentation files for webkitgtk6.0
BuildArch:      noarch
Requires:       webkitgtk6.0 = %{version}-%{release}
Obsoletes:      webkit2gtk5.0-help < %{version}-%{release}

%description -n webkitgtk6.0-help
This package contains developer documentation for webkitgtk6.0.
%endif

%package -n     jsc6.0
Summary:        JavaScript engine from webkitgtk6.0
Provides:       javascriptcoregtk6.0%{?_isa} = %{version}-%{release}
Obsoletes:      jsc5.0 < %{version}-%{release}

%description -n jsc6.0
This package contains JavaScript engine from webkitgtk6.0.


%package -n     jsc6.0-devel
Summary:        Development files for JavaScript engine from webkitgtk6.0
Provides:       javascriptcoregtk6.0-devel%{?_isa} = %{version}-%{release}
Requires:       javascriptcoregtk6.0%{?_isa} = %{version}-%{release}
Obsoletes:      jsc5.0-devel < %{version}-%{release}

%description -n jsc6.0-devel
The jsc6.0-devel package contains libraries, build data, and header
files for developing applications that use JavaScript engine from webkitgtk-6.0.



%prep
%autosetup -p1 -n webkitgtk-%{version}

%build
%cmake \
  -GNinja \
  -DPORT=GTK \
  -DCMAKE_BUILD_TYPE=Release \
  -DUSE_GTK4=ON \
  -DENABLE_WEBDRIVER=OFF \
  -DUSE_JPEGXL=OFF \
  -DUSE_AVIF=OFF \
  -DUSE_LIBBACKTRACE=OFF \
  -DUSE_GSTREAMER_TRANSCODER=OFF \
%if %{without docs}
  -DENABLE_DOCUMENTATION=OFF \
%endif
%if %{without gamepad}
  -DENABLE_GAMEPAD=OFF \
%endif
%ifarch aarch64 loongarch64
  -DENABLE_JIT=OFF \
  -DUSE_SYSTEM_MALLOC=ON \
%endif
%if 0%{?openEuler}
%ifarch aarch64
  -DUSE_64KB_PAGE_BLOCK=ON \
%endif
%endif
  %{nil}
%cmake_build

%install
%cmake_install

%find_lang WebKitGTK-6.0

# Finally, copy over and rename various files for %%license inclusion
%add_to_license_files Source/JavaScriptCore/COPYING.LIB
%add_to_license_files Source/ThirdParty/ANGLE/LICENSE
%add_to_license_files Source/ThirdParty/ANGLE/src/third_party/libXNVCtrl/LICENSE
%add_to_license_files Source/WebCore/LICENSE-APPLE
%add_to_license_files Source/WebCore/LICENSE-LGPL-2
%add_to_license_files Source/WebCore/LICENSE-LGPL-2.1
%add_to_license_files Source/WebInspectorUI/UserInterface/External/CodeMirror/LICENSE
%add_to_license_files Source/WebInspectorUI/UserInterface/External/Esprima/LICENSE
%add_to_license_files Source/WebInspectorUI/UserInterface/External/three.js/LICENSE
%add_to_license_files Source/WTF/icu/LICENSE
%add_to_license_files Source/WTF/wtf/dtoa/COPYING
%add_to_license_files Source/WTF/wtf/dtoa/LICENSE

%files -n webkitgtk6.0 -f WebKitGTK-6.0.lang
%license _license_files/*ThirdParty*
%license _license_files/*WebCore*
%license _license_files/*WebInspectorUI*
%license _license_files/*WTF*
%{_libdir}/libwebkitgtk-6.0.so.4*
%dir %{_libdir}/girepository-1.0
%{_libdir}/girepository-1.0/WebKit-6.0.typelib
%{_libdir}/girepository-1.0/WebKitWebProcessExtension-6.0.typelib
%{_libdir}/webkitgtk-6.0/
%{_libexecdir}/webkitgtk-6.0/
%exclude %{_libexecdir}/webkitgtk-6.0/MiniBrowser
%exclude %{_libexecdir}/webkitgtk-6.0/jsc


%files -n webkitgtk6.0-devel
%{_libexecdir}/webkitgtk-6.0/MiniBrowser
%{_includedir}/webkitgtk-6.0/
%exclude %{_includedir}/webkitgtk-6.0/jsc
%{_libdir}/libwebkitgtk-6.0.so
%{_libdir}/pkgconfig/webkitgtk-6.0.pc
%{_libdir}/pkgconfig/webkitgtk-web-process-extension-6.0.pc
%dir %{_datadir}/gir-1.0
%{_datadir}/gir-1.0/WebKit-6.0.gir
%{_datadir}/gir-1.0/WebKitWebProcessExtension-6.0.gir

%files -n jsc6.0
%license _license_files/*JavaScriptCore*
%{_libdir}/libjavascriptcoregtk-6.0.so.1*
%dir %{_libdir}/girepository-1.0
%{_libdir}/girepository-1.0/JavaScriptCore-6.0.typelib

%files -n jsc6.0-devel
%{_libexecdir}/webkitgtk-6.0/jsc
%dir %{_includedir}/webkitgtk-6.0
%{_includedir}/webkitgtk-6.0/jsc/
%{_libdir}/libjavascriptcoregtk-6.0.so
%{_libdir}/pkgconfig/javascriptcoregtk-6.0.pc
%dir %{_datadir}/gir-1.0
%{_datadir}/gir-1.0/JavaScriptCore-6.0.gir

%if %{with docs}
%files -n webkitgtk6.0-help
%dir %{_datadir}/doc
%{_datadir}/doc/javascriptcoregtk-6.0/
%{_datadir}/doc/webkitgtk-6.0/
%{_datadir}/doc/webkitgtk-web-process-extension-6.0/
%endif

%changelog
* Mon Mar 17 2025 lingsheng <lingsheng1@h-partners.com> - 2.46.6-2
- fix CVE-2025-24201

* Mon Feb 10 2025 lingsheng <lingsheng1@h-partners.com> - 2.46.6-1
- update to 2.46.6

* Mon Nov 18 2024 Funda Wang <fundawang@yeah.net> - 2.38.2-12
- adopt to new cmake macro

* Thu Aug 29 2024 lingsheng <lingsheng1@h-partners.com> - 2.38.2-11
- Modfiy loongarch64 and sw_64 support use all arch

* Thu Aug 22 2024 lingsheng <lingsheng1@h-partners.com> - 2.38.2-10
- fix CVE-2024-4558 CVE-2024-40779 CVE-2024-40780

* Mon Jul 22 2024 xinghe <xinghe2@h-partners.com> - 2.38.2-9
- fix build fails with libxml2 API change

* Wed Oct 11 2023 zhouwenpei <zhouwenpei1@h-partners.com> - 2.38.2-8
- fix check_install error

* Sun Oct 08 2023 zhouwenpei <zhouwenpei1@h-partners.com> - 2.38.2-7
- fix CVE-2023-39928

* Thu Aug 10 2023 xiasenlin <xiasenlin1@huawei.com> - 2.38.2-6
- split webkit2gtk5.0 from webkit2gtk3

* Tue Aug 08 2023 zhouwenpei <zhouwenpei1@h-partners.com> - 2.38.2-5
- Fix build with Ruby 3.2

* Mon May 29 2023 zhangpan <zhangpan103@h-partners.com> - 2.38.2-4
- fix CVE-2023-28204 CVE-2023-32373 CVE-2023-32409

* Fri Mar 17 2023 zhouwenpei <zhouwenpei1@h-partners.com> - 2.38.2-3
- strip binary files

* Thu Feb 16 2023 wenlong ding <wenlong.ding@turbolinux.com.cn> - 2.38.2-2
- Rename package to adaptor old Version

* Mon Dec 05 2022 lin zhang <lin.zhang@turbolinux.com.cn> - 2.38.2-1
- Update to 2.38.2 for gnome 43

* Tue Nov 29 2022 wuzx<wuzx1226@qq.com> - 2.36.3-3
- Add sw64 architecture

* Mon Nov 14 2022 huajingyun <huajingyun@loongson.cn> 2.36.3-2
- Add support loongarch

* Mon Jun 13 2022 lin zhang <lin.zhang@turbolinux.com.cn> 2.36.3-1
- Update to 2.36.3

* Fri Jun 10 2022 zhujunhao<zhujunhao11@huawei.com> - 2.32.4-4
- add wayland-porotocols-devel buildrequires

* Tue Jun 07 2022 houjinchang<houjinchang@huawei.com> - 2.32.4-3
- fix CVE-2022-30293 and CVE-2022-30294

* Thu Nov 04 2021 liuyumeng<liuyumeng5@huawei.com> - 2.32.4-2
- fix CVE-2021-42762

* Fri Oct 22 2021 zhanzhimin<zhanzhimin@huawei.com> - 2.32.4-1
- upgrade to 2.32.4

* Thu Jul 29 2021 wangkerong<wangkerong@huawei.com> - 2.32.1-2
- change xdg-desktop-protal-gts dependences

* Mon Jun 21 2021 wangkerong<wangkerong@huawei.com> - 2.32.1-1
- upgrade to 2.32.1

* Tue Dec 15 2020 hanhui<hanhui15@huawei.com> - 2.28.3-3
- modify license

* Wed Aug 05 2020 songnannan <songnannan2@huawei.com> - 2.28.3-2
- change the mesa-libELGS-devel to libglvnd-devel

* Thu Jul 23 2020 songnannan <songnannan2@huawei.com> - 2.28.3-1
- Type:enhancement
- Id:NA
- SUG:NA
- DESC: update to  2.28.3

* Mon Feb 24 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.22.2-6
- Type:enhancement
- Id:NA
- SUG:NA
- DESC:fix rpmbuild fail in make

* Thu Jan 23 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.22.2-5
- Type:enhancement
- Id:NA
- SUG:NA
- DESC:close build option gtkdoc

* Sat Jan 11 2020 openEuler Buildteam <buildteam@openeuler.org> - 2.22.2-4
- Type:enhancement
- Id:NA
- SUG:NA
- DESC:optimization the spec

* Tue Dec 31 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.22.2-3
- Enable gtk-doc and go-introspection

* Fri Nov 8 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.22.2-2
- Modify cmake option to disable gtk-doc and go-introspection

* Wed Sep 18 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.22.2-1
- Package init
